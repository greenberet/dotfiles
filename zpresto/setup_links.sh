
echo $HOME
rm $HOME/.zlogin
rm $HOME/.zlogout
rm $HOME/.zpreztorc
rm $HOME/.zprofile
rm $HOME/.zshenv
rm $HOME/.zshrc
rm  -rf $HOME/.zprezto
unlink $HOME/.zlogin
unlink $HOME/.zlogout
unlink $HOME/.zpreztorc
unlink $HOME/.zprofile
unlink $HOME/.zshenv
unlink $HOME/.zshrc
unlink $HOME/.zprezto

ln -s ~/dotfiles/zpresto/runcoms/zlogin $HOME/.zlogin
ln -s ~/dotfiles/zpresto/runcoms/zlogout $HOME/.zlogout
ln -s ~/dotfiles/zpresto/runcoms/zpreztorc $HOME/.zpreztorc
ln -s ~/dotfiles/zpresto/runcoms/zprofile $HOME/.zprofile
ln -s ~/dotfiles/zpresto/runcoms/zshenv $HOME/.zshenv
ln -s ~/dotfiles/zpresto/runcoms/zshrc $HOME/.zshrc
ln -s ~/dotfiles/zpresto $HOME/.zprezto
