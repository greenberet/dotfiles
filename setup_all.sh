rm ~/.tmux.conf
rm ~/.vimrc
rm -rf ~/.vim
unlink ~/.tmux.conf
unlink ~/.vimrc
unlink ~/.vim

ln -s ~/dotfiles/tmux/tmux.conf ~/.tmux.conf
ln -s ~/dotfiles/vim/vimrc ~/.vimrc
ln -s ~/dotfiles/vim ~/.vim

bash ~/dotfiles/zpresto/setup_links.sh

chsh -s $(which zsh)

