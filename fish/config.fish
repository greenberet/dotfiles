
function cgrep
  find . -exec grep -i "$argv[1]" {} /dev/null \; 2> /dev/null
end

#pastebin
alias sprung="curl -F 'sprunge=<-' http://sprunge.us "

# Cuda
set -x  CUDA_HOME /usr/local/cuda-7.0/
set -x  LD_LIBRARY_PATH $CUDA_HOME/lib64 
alias gtop="nvidia-smi -q | grep -i -A 2 utili"
  

# Customize to your needs...
set -x  EDITOR "vim" 
alias vi="vim"
alias hive='/usr/local/prism/bin/hive --namespace ads'
alias up='git fetch; git rebase trunk; ab'  
alias killtunnels="pkill -f 'ssh.*\-L.*'"

# Git
alias gb='git branch'
alias ga='git commit --amend'
alias gs='git status'
alias gc='git checkout' 

function gn 
  git checkout -b "$argv[1]" master
end

alias gr='git branch -m' 
alias gd='git branch -D' 

alias tmux="tmux -2"

alias dayago='date --d "1 day ago" +%s'

set -x  LUA_PATH '/home/sviyer/.luarocks/share/lua/5.1/?.lua;/home/sviyer/.luarocks/share/lua/5.1/?/init.lua;/home/sviyer/torch/install/share/lua/5.1/?.lua;/home/sviyer/torch/install/share/lua/5.1/?/init.lua;./?.lua;/home/sviyer/torch/install/share/luajit-2.1.0-alpha/?.lua;/usr/local/share/lua/5.1/?.lua;/usr/local/share/lua/5.1/?/init.lua'
set -x  LUA_CPATH '/home/sviyer/.luarocks/lib/lua/5.1/?.so;/home/sviyer/torch/install/lib/lua/5.1/?.so;./?.so;/usr/local/lib/lua/5.1/?.so;/usr/local/lib/lua/5.1/loadall.so'
set -x  PYTHONPATH /home/sviyer/django17/lib/python2.7/site-packages/:/home/sviyer/nlidb/src/
set -x  PATH $PATH /home/sviyer/torch/install/bin/

set -x  CLASSPATH ".:/home/sviyer/nlidb/data/miltos/antlr-4.5.2-complete.jar"
alias antlr4='java -jar /home/sviyer/nlidb/data/miltos/antlr-4.5.2-complete.jar'
alias grun='java org.antlr.v4.gui.TestRig'


